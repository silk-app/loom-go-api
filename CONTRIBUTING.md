# Contributing
Guidelines for contributing to Loom Go API

## Branches
**master**: main development branch  
**staging**: development branch for new features  

## Commits
Commits must have a meaningful message of **all** changes in the commit.  
Commit messages should not be much longer than 50 characters per line.  

## Pull requests
Pull requests should be made to the appropriate branch:
- new fatures / major feature changes: staging
- bug fixes / minor feature changes: master
